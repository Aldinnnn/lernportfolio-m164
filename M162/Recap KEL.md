## Fragen und Antworten

### 1. Stufen der Wissenstreppe

- **Daten**: Unbearbeitete Rohinformationen, zum Beispiel in Form von Zahlen oder Texten.
  
- **Zeichen**: Symbole oder Einheiten, die Daten repräsentieren, z. B. Buchstaben, Zahlen oder andere Symbole.

- **Informationen (Bezug zu Menschen)**: Daten, die in einem bestimmten Kontext interpretiert wurden und für Menschen verständlich sind.

- **Wissen**: Informationen, die organisiert, verarbeitet und verstanden wurden und in einer Weise angewendet werden können, die Entscheidungen beeinflusst.

- **Kompetenzen**: Die Fähigkeit, Wissen effektiv in unterschiedlichen Situationen anzuwenden.

- **Handeln**: Die tatsächliche Umsetzung von Wissen und Kompetenzen in Handlungen oder Entscheidungen.

### 2. Wie werden Netzwerk-Beziehungen im logischen Modell abgebildet?

Netzwerkbeziehungen werden im logischen Modell durch die Verwendung von Verbindungstabellen dargestellt. Diese Tabellen enthalten Fremdschlüssel, um die Beziehungen zwischen verschiedenen Entitäten in der Datenbank zu definieren.

### 3. Was sind Anomalien in einer Datenbasis? Welche Arten gibt es?

Anomalien in einer Datenbasis sind Probleme oder Inkonsistenzen, die auftreten können, wenn Daten nicht ordnungsgemäß organisiert oder strukturiert sind. Die drei Arten von Anomalien sind:

- **Einfügeanomalie**: Diese tritt auf, wenn es nicht möglich ist, neue Daten hinzuzufügen, ohne auch andere Daten hinzufügen zu müssen, die in keiner Beziehung stehen.

- **Löschungsanomalie**: Eine Löschungsanomalie liegt vor, wenn das Löschen von Daten unbeabsichtigte Verluste anderer Daten verursacht, die eigentlich beibehalten werden sollten.

- **Änderungsanomalie**: Änderungsanomalien treten auf, wenn Änderungen an einer Instanz eines Datensatzes nicht konsistent durchgeführt werden und dies zu Inkonsistenzen führt.

### 4. Gibt es redundante "Daten"? Warum?

Ja, redundante Daten sind vorhanden, wenn dieselben Dateninformationen mehrmals gespeichert sind. Dies kann zu Problemen wie erhöhtem Speicherplatzbedarf, Inkonsistenzen und erhöhtem Wartungsaufwand führen. Redundante Daten können auftreten, wenn Daten nicht ordnungsgemäß strukturiert sind oder wenn Datenbanken nicht normalisiert wurden.

### 5. Datenstrukturierung:  Welche zwei Aspekte können strukturiert werden?  Welche Kategorien (Abstufungen) gibt es bei der Strukturierung?  Und wie müssen die Daten in einer DB strukturiert sein?

Die beiden Aspekte, die strukturiert werden können, sind Datenorganisation und Datenrepräsentation.

Die Kategorien der Strukturierung umfassen:

- **Physische Strukturierung**: Die Art und Weise, wie Daten auf der Festplatte oder im Speicher gespeichert werden.

- **Logische Strukturierung**: Die Organisation von Daten in einer Datenbank in Tabellen, Schemata und Beziehungen.

Die Daten in einer Datenbank müssen entsprechend den Prinzipien der Datenbanknormalisierung strukturiert sein, um Redundanzen zu minimieren, Anomalien zu vermeiden und die Datenintegrität sicherzustellen.

### 6. Terminologie 

Das Bild zeigt eine Entity-Relationship-Diagramm (ER-Diagramm), das die Struktur und die Beziehungen zwischen verschiedenen Entitäten in einer Datenbank darstellt. Es umfasst Entitäten (dargestellt durch Rechtecke), Attribute (dargestellt durch Ovale) und Beziehungen zwischen Entitäten (dargestellt durch Verbindungslinien mit Verbindungssymbolen, die die Art der Beziehung anzeigen, wie z. B. "1" für One-to-One und "N" für Many-to-Many).

![Mitarbeitertabelle](https://gitlab.com/ch-tbz-it/Stud/m164/-/raw/main/10_Auftraege_und_Uebungen/00_Start/Recap_Fragen/Tabelle_labelled.png "Mitarbeitertabelle")
