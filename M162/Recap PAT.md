## Fragen und Antworten

### 1. Was sind Constraints?

Constraints sind Regeln oder Bedingungen, die auf Datenbanktabellen angewendet werden, um die Integrität und Konsistenz der Daten zu gewährleisten.

### 2. Zählen Sie diese auf und erklären Sie.

Es gibt verschiedene Arten von Constraints:

- **Primärschlüsselconstraint**: Definiert eine Spalte oder eine Gruppe von Spalten, die eindeutig sind und jede Zeile in der Tabelle eindeutig identifizieren.
  
- **Fremdschlüsselconstraint**: Legt fest, dass die Werte in einer Spalte (oder Spalten) einer Tabelle auf Werte in einer anderen Tabelle verweisen müssen.

- **Unique Constraint**: Stellt sicher, dass alle Werte in einer Spalte oder einer Gruppe von Spalten eindeutig sind.

- **Check Constraint**: Überprüft, ob Werte in einer Spalte oder einer Gruppe von Spalten bestimmten Bedingungen entsprechen.

### 3. Zählen Sie die vier Beziehungstypen und deren Bedeutung auf.

Die vier Beziehungstypen sind:

- **One-to-One (1:1)**: Eine Zeile in einer Tabelle ist mit genau einer Zeile in einer anderen Tabelle verbunden.

- **One-to-Many (1:N)**: Eine Zeile in einer Tabelle kann mit mehreren Zeilen in einer anderen Tabelle verbunden sein.

- **Many-to-One (N:1)**: Mehrere Zeilen in einer Tabelle können mit einer einzigen Zeile in einer anderen Tabelle verbunden sein.

- **Many-to-Many (N:M)**: Mehrere Zeilen in einer Tabelle können mit mehreren Zeilen in einer anderen Tabelle verbunden sein.

### 4. Was bedeutet referentielle Integrität in einer Datenbank?

Referentielle Integrität bezieht sich auf die Sicherstellung, dass Beziehungen zwischen Tabellen durch gültige Werte in den verknüpften Spalten erhalten bleiben. Dies wird oft durch die Verwendung von Fremdschlüsselconstraints erreicht, um sicherzustellen, dass Werte in einer Spalte einer Tabelle auf gültige Werte in einer anderen Tabelle verweisen.

### 5. Terminologie 

Das Bild zeigt eine Entity-Relationship-Diagramm (ER-Diagramm), das die Struktur und die Beziehungen zwischen verschiedenen Entitäten in einer Datenbank darstellt. Es umfasst Entitäten (dargestellt durch Rechtecke), Attribute (dargestellt durch Ovale) und Beziehungen zwischen Entitäten (dargestellt durch Verbindungslinien mit Verbindungssymbolen, die die Art der Beziehung anzeigen, wie z. B. "1" für One-to-One und "N" für Many-to-Many).

### 6. Bild beschreiben

1. **Tabellenname**
 
2. **Datensatz**
 
3. **Attribute**
 
4. **Wert**
 
5. **ERD**

![Mitarbeitertabelle](https://gitlab.com/ch-tbz-it/Stud/m164/-/raw/main/10_Auftraege_und_Uebungen/00_Start/Recap_Fragen/Tabelle_labelled.png "Mitarbeitertabelle")
