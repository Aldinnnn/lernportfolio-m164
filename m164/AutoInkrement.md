# Autoinkrement in SQL

Autoinkrement ist ein Konzept in SQL, das verwendet wird, um automatisch einen eindeutigen Wert für eine Spalte zu generieren, normalerweise als Primärschlüssel für eine Tabelle. Dies ermöglicht es, neue Datensätze in die Tabelle einzufügen, ohne explizit einen Wert für die Spalte anzugeben.

In SQL wird Autoinkrement häufig durch die Verwendung des `AUTO_INCREMENT`-Attributs erreicht. Dieses Attribut wird normalerweise mit dem Datentyp `INT` oder `BIGINT` für numerische Spalten verwendet. Wenn eine Spalte mit `AUTO_INCREMENT` deklariert wird, erhöht die Datenbank den Wert automatisch für jede neue Zeile, die in die Tabelle eingefügt wird.

Beispiel in MySQL:

```sql
CREATE TABLE Kunden (
    KundenID INT AUTO_INCREMENT PRIMARY KEY,
    Vorname VARCHAR(50),
    Nachname VARCHAR(50)
);
