# Datenbank Management System (DBMS)

Ein **Datenbanksystem (DBS)** ist ein System zur elektronischen Datenverwaltung, das große Datenmengen effizient, widerspruchsfrei und dauerhaft speichert und benötigte Teilmengen in verschiedenen Darstellungsformen für Benutzer und Anwendungsprogramme bereitstellt. Es besteht aus zwei Teilen: der Verwaltungssoftware, genannt **Datenbankmanagementsystem (DBMS)**, und der Menge der zu verwaltenden Daten, der eigentlichen **Datenbank (DB)**.

***Merkmale eines DBMS:***
- Integrierte Datenhaltung
- Sprache (Query Language)
- Katalog (Data Dictionary)
- Benutzersichten (Views)
- Konsistenzkontrolle (Integritätssicherung)
- Datenzugriffskontrolle (Access Control)
- Transaktionen
- Mehrbenutzerfähigkeit
- Datensicherung

**Vorteile des Datenbankeinsatzes:**
- Nutzung von Standards
- Effizienter Datenzugriff
- Kürzere Softwareentwicklungszeiten
- Hohe Flexibilität (Datenunabhängigkeit)
- Hohe Verfügbarkeit
- Große Wirtschaftlichkeit

**Nachteile von Datenbanksystemen:**
- Hohe Anfangsinvestitionen
- Weniger effizient für spezialisierte Anwendungen
- Einschränkungen bei konkurrierenden Anforderungen
- Mehrkosten für Sicherheit und Synchronisation
- Erfordert hochqualifiziertes Personal
- Verwundbarkeit durch Zentralisierung

**Fachbegriffe:**
- Datenbanksystem (DBS)
- Datenbankmanagementsystem (DBMS)
- Datenbank (DB)
- Query Language
- Katalog (Data Dictionary)
- Benutzersichten (Views)
- Konsistenzkontrolle (Integritätssicherung)
- Datenzugriffskontrolle (Access Control)
- Transaktionen
- Mehrbenutzerfähigkeit
- Datensicherung
- Datenunabhängigkeit


[](https://gitlab.com/ch-tbz-it/Stud/m164/-/raw/main/02_Theorie_Unterlagen/DDL/DDL.png)
