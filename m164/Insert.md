## INSERT INTO in SQL

`INSERT INTO` ist eine SQL-Anweisung, die verwendet wird, um neue Datensätze in eine Datenbanktabelle einzufügen. Die Syntax lautet:


Hier ist eine Erklärung der verschiedenen Teile dieser Anweisung:

- `INSERT INTO`: Dies ist der Anfang der SQL-Anweisung, der angibt, dass Sie Daten in eine Tabelle einfügen möchten.
  
- `table_name`: Dies ist der Name der Tabelle, in die Sie Daten einfügen möchten.

- `(column1, column2, column3, ...)`: Hier geben Sie die Spalten an, in die Sie Daten einfügen möchten. Sie können entweder den Namen aller Spalten in der Tabelle angeben oder nur die Spalten, für die Sie Werte einfügen möchten.

- `VALUES (value1, value2, value3, ...)`: Dieser Teil enthält die tatsächlichen Werte, die in die entsprechenden Spalten eingefügt werden sollen. Die Reihenfolge der Werte muss mit der Reihenfolge der Spalten übereinstimmen. Jeder Wert entspricht der jeweiligen Spalte, die in der vorherigen Klammer definiert wurde.

### Beispiel:

Angenommen, Sie haben eine Tabelle mit dem Namen "customers", die folgende Spalten hat: "customer_id", "first_name", "last_name" und "email". Wenn Sie einen neuen Kunden in diese Tabelle einfügen möchten, würden Sie folgendermaßen vorgehen:

