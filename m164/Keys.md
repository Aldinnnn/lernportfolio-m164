
## Primary Key

Ein Primary Key ist eine eindeutige Kennzeichnung für jede Zeile (Datensatz) in einer Datenbanktabelle. Er wird verwendet, um sicherzustellen, dass jede Zeile eindeutig identifizierbar ist. Der Primary Key kann aus einer oder mehreren Spalten bestehen und wird verwendet, um auf einzelne Datensätze zuzugreifen und Beziehungen zwischen verschiedenen Tabellen herzustellen.

Beispiel:

```sql
CREATE TABLE Benutzer (
    BenutzerID INT PRIMARY KEY,
    Vorname VARCHAR(50),
    Nachname VARCHAR(50)
);
```

In diesem Beispiel ist `BenutzerID` der Primary Key der Tabelle "Benutzer". Jede Zeile in der Tabelle wird durch eine eindeutige `BenutzerID` identifiziert.

## Foreign Key

Ein Foreign Key ist eine Spalte oder eine Gruppe von Spalten in einer Tabelle, die auf den Primary Key in einer anderen Tabelle verweisen. Der Foreign Key stellt eine Beziehung zwischen den beiden Tabellen her. Er wird verwendet, um die Integrität der Daten sicherzustellen und Referential Integrity zu gewährleisten.

Beispiel:

```sql
CREATE TABLE Bestellungen (
    BestellID INT PRIMARY KEY,
    ProduktID INT,
    BenutzerID INT,
    FOREIGN KEY (ProduktID) REFERENCES Produkte(ProduktID),
    FOREIGN KEY (BenutzerID) REFERENCES Benutzer(BenutzerID)
);
```

In diesem Beispiel sind `ProduktID` und `BenutzerID` Foreign Keys in der Tabelle "Bestellungen", die auf die entsprechenden Primary Keys in den Tabellen "Produkte" und "Benutzer" verweisen.
