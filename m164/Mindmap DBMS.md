# Mindmap: Datenbank Management System (DBMS)

- **Datenbanksystem (DBS)**
  - System zur elektronischen Datenverwaltung
- **Datenbank Management System (DBMS)**
  - Verwaltungssoftware für die Datenbank
- **Datenbank (DB)**
  - Menge der zu verwaltenden Daten
- **Query Language**
  - Sprache für Datenabfragen und -manipulation
- **Katalog (Data Dictionary)**
  - Enthält Metadaten zur Datenbankstruktur
- **Benutzersichten (Views)**
  - Unterschiedliche Sichten auf den Datenbestand für verschiedene Benutzer
- **Konsistenzkontrolle**
  - Gewährleistung der Korrektheit der Datenbankinhalte und -änderungen
- **Datenzugriffskontrolle (Access Control)**
  - Regelung des Zugriffs auf die Datenbank
- **Transaktionen**
  - Logisch zusammengehörige Datenbankänderungen
- **Mehrbenutzerfähigkeit**
  - Synchronisation von Transaktionen mehrerer Benutzer

## Produkte
- **Adabas** (Software AG)
- **Cache** (InterSystems)
- **DB2** (IBM)
- **Firebird**
- **IMS** (IBM)
- **Informix** (IBM)
- **InterBase** (Borland)
- **MS Access** (Microsoft)
- **MS SQL Server** (Microsoft)
- **MySQL** (MySQL AB)
- **Oracle** (ORACLE)
- **PostgreSQL**
- **Sybase ASE** (Sybase)
- **Versant**
- **Visual FoxPro** (Microsoft)
- **Teradata** (NCR Teradata)

## DB-Engine Ranking (Top 10)

1. **Oracle**
   - Typ: Relational, Multi-model
   - Punktzahl: 1241.45

2. **MySQL**
   - Typ: Relational, Multi-model
   - Punktzahl: 1106.67

3. **Microsoft SQL Server**
   - Typ: Relational, Multi-model
   - Punktzahl: 853.57

4. **PostgreSQL**
   - Typ: Relational, Multi-model
   - Punktzahl: 629.41

5. **MongoDB**
   - Typ: Document, Multi-model
   - Punktzahl: 420.36

6. **Redis**
   - Typ: Key-value, Multi-model
   - Punktzahl: 160.71

7. **Elasticsearch**
   - Typ: Search engine, Multi-model
   - Punktzahl: 135.74

8. **IBM Db2**
   - Typ: Relational, Multi-model
   - Punktzahl: 132.23

9. **Snowflake**
   - Typ: Relational
   - Punktzahl: 127.45

10. **SQLite**
   - Typ: -
   - Punktzahl: -


