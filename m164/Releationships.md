# Identifying Relationship vs. Non-Identifying Relationship

Zur Unterscheidung der **identifying relationship** von der **non-identifying relationship** gibt es oft Verwirrung. Eine kurze, einfache Erklärung findet sich [hier](http://www.datenbank-grundlagen.de/beziehungen-datenbanken.html).

- **Identifying Relationship**: Der Fremdschlüssel ist Teil des Identifikationsschlüssels. Die ID besteht aus mehreren (mindestens 2) Attributen, wobei der Datensatz der referenzierenden Tabelle sich teilweise durch den referenzierten Datensatz identifiziert. Ein Beispiel ist ein Raum, der den Fremdschlüssel auf das zugehörige Gebäude in seiner ID hat. Bei einer identifying relationship wäre es unklug, den Fremdschlüsselwert zu ändern und den Raum einem anderen Gebäude zuzuordnen.

- **Non-Identifying Relationship**: Ein Beispiel hierfür ist die Beziehung Mitarbeiter/Abteilung. Der Fremdschlüssel auf den Arbeitgeber gehört nicht zur Identität einer Person, da sich der Arbeitgeber jederzeit ändern kann.
