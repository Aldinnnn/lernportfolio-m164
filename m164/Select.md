## SELECT-Befehl in SQL

Der SELECT-Befehl in SQL wird verwendet, um Daten aus einer oder mehreren Tabellen einer Datenbank abzurufen. Er ermöglicht es, bestimmte Spalten auszuwählen, Filterbedingungen festzulegen und die zurückgegebenen Ergebnisse zu sortieren.

### Syntax:

Die grundlegende Syntax des SELECT-Befehls lautet:

```sql
SELECT Spaltenliste
FROM Tabellenname
WHERE Bedingung
ORDER BY Spalte ASC/DESC;
